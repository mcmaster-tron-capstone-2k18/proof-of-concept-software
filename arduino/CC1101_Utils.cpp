#include <Arduino.h>
#include <cc1101.h>
#include <ccpacket.h>

#include "CC1101_Utils.h"

const char *CC1101_INFO_DUMP_STRING =
"CC1101:\r\n"
"Part Number: %i\r\n"
"Version: %i\r\n"
"MarcState: %i\r\n"
"Test 0: %i\r\n"
"Test 1: %i\r\n"
"Test 2: %i\r\n";

/**
 *
 * @param syncWord Pointer to 2 byte array
 * @param frequency: One of [CFREQ_868, CFREQ_915,CFREQ_433, CFREQ_918, CFREQ_LAST]
 * @param power: One of [PA_LowPower, PA_LongDistance]
 */
void cc1101_easy_init(CC1101 radio, uint8_t *syncWord, uint8_t frequency, uint8_t power) {
    radio.init();
    radio.setSyncWord(syncWord);
    radio.setCarrierFreq(frequency);
    radio.disableAddressCheck();
    radio.setTxPowerAmp(power);
}

/**
 *
 * @param buf: Buffer to fill with information
 */
void cc1101_dump_info(CC1101 radio, char buf[]) {
    sprintf(buf, CC1101_INFO_DUMP_STRING,
            radio.readReg(CC1101_PARTNUM, CC1101_STATUS_REGISTER),
            radio.readReg(CC1101_VERSION, CC1101_STATUS_REGISTER),
            radio.readReg(CC1101_MARCSTATE, CC1101_STATUS_REGISTER) & 0x1f,
            radio.readReg(CC1101_TEST0, CC1101_STATUS_REGISTER),
            radio.readReg(CC1101_TEST1, CC1101_STATUS_REGISTER),
            radio.readReg(CC1101_TEST2, CC1101_STATUS_REGISTER)
            );
}

// Get signal strength indicator in dBm.
// See: http://www.ti.com/lit/an/swra114d/swra114d.pdf
int cc1101_rssi(char raw) {
    uint8_t rssi_dec;
    // TODO: This rssi_offset is dependent on baud and MHz; this is for 38.4kbps and 433 MHz.
    uint8_t rssi_offset = 74;
    rssi_dec = (uint8_t) raw;
    if (rssi_dec >= 128)
        return ((int)( rssi_dec - 256) / 2) - rssi_offset;
    else
        return (rssi_dec / 2) - rssi_offset;
}

// Get link quality indicator.
int cc1101_lqi(char raw) {
    return 0x3F - raw;
}
